<?php

namespace site\Http\Controllers;

use Illuminate\Http\Request;

use site\Http\Requests;
use site\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Pagina inicial do site.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    } 
    
    public function sobre()
    {
        return view('sobre');
    }
    
    public function contato(){
        return view('contato');
    }

}