<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>::Wilson Lucena::</title>
    <link href="css/mestro.css" rel="stylesheet" type="text/css" />
    <link href="fonts/stylesheet.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="css/banner_style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="pro_drop_1/pro_drop_1.css" />

    <script src="pro_drop_1/stuHover.js" type="text/javascript"></script>

</head>
<body>

<div id="wrap">
    <!-- Top Section -->
    <div class="top_sec">
        <div class="container">
            <ul><li class="number">(61) 8556-7287</li></ul>
            <div class="social">
                <a href="#." class="fb"></a>
                <a href="#." class="skype"></a>
            </div>
        </div>
    </div>
    <!-- Top Section End -->

    <!-- Header -->
    <div id="header">
        <div class="container">
            <img class="logo" src="images/logo.png" alt="" />

            <!-- Navigation -->
            <div class="nav">
                <ul id="nav">
                    <li class="divider"><a class="selected" href="/">Home</a></li>
                    <li class="divider"><a href="/sobre">Sobre</a></li>
                    {{--<li class="top divider"><a href="#nogo2" id="products" class="top_link">Portfolio</a></li>--}}
                    {{--<li class="top divider"><a href="blog.html" id="products" class="top_link">Blog</a></li>--}}
                    <li class="divider"><a href="/contato">Contato</a></li>
                </ul>
            </div>
            <!-- Navigation End -->


        </div>
    </div>
    <!-- Header End -->

    <!-- SliderShow -->
                    @yield('slide')
    <!-- Fim SlideShow -->
    <!-- Content -->
    <!-- Sub banner -->
    <div id="sub_banner">
        <div class="container">
            <div class="text">
                @yield('banner-titulo')
            </div>
        </div>
    </div>
    <div id="content">
        <div class="container">
                     @yield('conteudo')
        </div>
    </div>
    <div class="clear"></div>
    <!-- Content End -->

    <!-- Footer -->
    <div id="footer">
        <div class="clear"></div>
        <div class="copyright">
            <div class="container">
                <p>Copyright 2016 @ Densenvolvido por Wilson Desenvolvedor PHP</p>
                <div class="social">
                    <a href="#." class="fb"></a>
                    <a href="#." class="skype"></a>
                </div>

            </div>
        </div>
        <div class="clear"></div>

        <div class="container">
            <img class="fotter_logo" src="images/footer_logo.jpg" alt="" />
            {{--<span class="bot_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. the  Lorem Ipsum has been  	 		the industry's standard dummy text ever since the you. Lorem Ipsum is simply dummy text of the printing and typesetting  	 	 		industry. the  Lorem Ipsum has been the industry's standard dummy text ever since the you. </span>--}}
        </div>
        <div class="clear"></div>
    </div>
    <!-- Footer End -->

</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- jQuery KenBurn Slider  -->
<script type="text/javascript" src="js/jquery.mestro.plugins.min.js"></script>
<script type="text/javascript" src="js/jquery.mestro.revolution.min.js"></script>
<script type="text/javascript">

    var tpj=jQuery;
    tpj.noConflict();

    tpj(document).ready(function() {

        if (tpj.fn.cssOriginal!=undefined)
            tpj.fn.css = tpj.fn.cssOriginal;

        tpj('.fullwidthbanner').revolution(
                {
                    delay:10000,
                    startwidth:890,
                    startheight:550,

                    onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

                    thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                    thumbHeight:50,
                    thumbAmount:3,

                    hideThumbs:200,
                    navigationType:"none",					//bullet, thumb, none, both	 (No Shadow in Fullwidth Version !)
                    navigationArrows:"verticalcentered",		//nexttobullets, verticalcentered, none
                    navigationStyle:"square",				//round,square,navbar

                    touchenabled:"on",						// Enable Swipe Function : on/off

                    navOffsetHorizontal:0,
                    navOffsetVertical:20,

                    fullWidth:"on",

                    shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

                    stopLoop:"off"							// on == Stop loop at the last Slie,  off== Loop all the time.

                });




    });

</script>
</body>
</html>