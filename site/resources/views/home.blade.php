@extends('template')
@section('slide')
    <!-- Banner -->
    <div id="banner">
        <div class="fullbanner">
            <div class="fullwidthbanner">
                <ul>

                    <li data-transition="fade" data-slotamount="15" data-thumb="images/revolution/slides/thumb4.jpg"> <img src= 	 	   	   	 	 		"images/banners/banner1.jpg">
                        <div class="caption sfb" data-x="410" data-y="80" data-speed="3000" data-start="900" data-easing="easeOutBack"><img src= 	 	 		"images/banners/banner_img1.png"></div>

                        <div class="caption lft small_text"  data-x="-40" data-y="160" data-speed="300" data-start="2600" data-easing="easeOutExpo"> 	 		<span class="blue_text">SITES e</span></div>

                        <div class="caption lft small_text"  data-x="-40" data-y="235" data-speed="300" data-start="2900" data-easing="easeOutExpo"> 	 		<span class="green_text">SISTEMAS</span></div>

                        <div class="caption lft small_text"  data-x="-40" data-y="320" data-speed="300" data-start="3200" data-easing="easeOutExpo"> 	 		<span class="pink_text">WEB</span></div>

                        {{--<div class="caption lft small_text"  data-x="-30" data-y="370" data-speed="300" data-start="3500" data-easing="easeOutExpo"> 	 		<span class="small_text1">Oferecemos oportunidade unica de uma experiencia surreal na web</span></div>--}}
                    </li>
                    <div class="clear"></div>

                    <li data-transition="slideup" data-slotamount="10" data-thumb="images/revolution/thumbs/thumb2.jpg">  <img src= 	 	   	 	 	 		"images/banners/banner2.jpg">
                        <div class="caption lft" data-x="0" data-y="205" data-speed="900" data-start="900" data-easing="easeOutBounce"><img src= 	 	 		"images/banners/banner_lcd.png"></div>
                        <div class="caption lft" data-x="250" data-y="365" data-speed="1500" data-start="1500" data-easing="easeOutBounce"><img src= 	 	 		"images/banners/banner_mac.png"></div>
                        <div class="caption lft" data-x="-40" data-y="365" data-speed="2100" data-start="2100" data-easing="easeOutBounce"><img src= 	 	 		"images/banners/banner_ipad.png"></div>
                        <div class="caption lft" data-x="510" data-y="432" data-speed="2700" data-start="2700" data-easing="easeOutBounce"><img src= 	 	 		"images/banners/banner_mb.png"></div>


                        <div class="caption lft small_text"  data-x="510" data-y="100" data-speed="300" data-start="2600" data-easing="easeOutExpo"> 			<span class="white_text1">Designer</span></div>

                        <div class="caption lft small_text"  data-x="510" data-y="165" data-speed="600" data-start="2900" data-easing="easeOutExpo"> 	 		<span class="white_text2">Criativos e</span></div>

                        <div class="caption lft small_text"  data-x="510" data-y="225" data-speed="1300" data-start="3200" data-easing="easeOutExpo"> 	 		<span class="white_text3">Responsivos</span></div>

                        {{--<div class="caption lft small_text"  data-x="510" data-y="298" data-speed="1800" data-start="3500" data-easing="easeOutExpo"> 			<span class="white_text4">Criativos</span></div>--}}

                    </li>
                    <div class="clear"></div>

                    {{--<li data-transition="fade" data-slotamount="5" ><img src="images/banners/banner3.jpg">--}}
                        {{--<div class="caption lfr" data-x="400" data-y="184" data-speed="900" data-start="900" data-easing="easeOutExpo"><img src= 	 			"images/banners/banner_img3.png"></div>--}}

                        {{--<div class="caption lft small_text"  data-x="-40" data-y="195" data-speed="300" data-start="2600" data-easing="easeOutExpo"> 	 		<span class="blue_text3">we are <span class="orange_text">ready </span> </span></div>--}}

                        {{--<div class="caption lft small_text"  data-x="-40" data-y="255" data-speed="300" data-start="2900" data-easing="easeOutExpo"> 	 		<span class="gray_text">to <span class="green_text3">design</span> your</span></div>--}}
                        {{--<div class="clear"></div>--}}
                        {{--<div class="caption lft small_text"  data-x="-40" data-y="325" data-speed="300" data-start="3200" data-easing="easeOutExpo"> 	 		<span class="pink_text3">website</span></div>--}}

                    {{--</li>--}}
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>

    </div>
    <div class="clear"></div>
    <!-- Banner End -->

    <!-- Front Text -->
    <div id="front_text">
        <div class="container">
            <div class="text">
                <h1>"Os verdadeiros artistas criam coisas reais e que serão usadas."</h1>
            </div>
        </div>
    </div>
    <!-- Front Text End -->
@stop
@section('conteudo')
@stop
