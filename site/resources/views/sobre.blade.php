
@extends('template')
<!-- Sub banner End -->
@section('banner-titulo')
    <h2>Minha História</h2>
@stop
<!-- Sub banner End -->

@section('conteudo')
    <div id="sub_content">
    <div class="container">
        <div class="what_do">
            <img class="about_img" src="images/about_img.jpg" alt="" />
            <div class="what_text">
                    <p>Olá meu nome é Wilson Lucena , sou Desenvolvedor PHP me dedico a profissão desde de 2012
                    amo o que faço não sou um mágico do PHP mas posso dizer que sou um verdadeiro "artesão" da linguagem,
                    sou graduado em Análise e Desenvolvimento de Sistemas - UDF.
                        Desenvolvo projetos voltados para o posicionamento e fortalecimento do cliente na web, através da
                        análise concorrencial e das melhores estratégias, utilizando sempre tecnologias avançadas e ideias
                        inovadoras.


                    </p>
                Algumas das minhas competencias:

                <ul>
                    <li>PHP 5</li>
                    <li>Mysql e Oracle</li>
                    <li>Framework Symfony 2</li>
                    <li>Framework Laravel</li>
                    <li>Bootstrap 3</li>
                    <li>Git </li>
                    <li>Bower</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear"></div>

<div class="container">
    <img class="clients" src="images/client_img1.jpg" alt="" />
</div>
<div class="clear"></div>

@stop